﻿#include <iostream>
#include <string>

using namespace std;

// индексы для строк
enum{One, Two, Three, AfterThree, End, Err};
// индексы для столбцов
enum{Ch_one, Ch_two, Ch_three, Other};

/*

           |   Ch_one   |   Ch_two   |  Ch_three  |    Other   | 
----------------------------------------------------------------   
One		   |     One    |    Two     |   Three    |    Err     |
----------------------------------------------------------------
Two		   |     End    |    Two     |   Three    |    Err     |
----------------------------------------------------------------
Three      |     End    | AfterThree |   Three    |    Err     |
----------------------------------------------------------------
AfterThree |     End    |    Err     |    Err     |    Err     |
----------------------------------------------------------------
End		   |    -1      |    -1      |    -1      |    -1      |
----------------------------------------------------------------
Err		   |    -1      |    -1      |    -1      |    -1      |
----------------------------------------------------------------
*/


int main() {
	int state_machine[6][4];

	// состояние 1
	state_machine[0][0] = One;
	state_machine[0][1] = Two;
	state_machine[0][2] = Three;
	state_machine[0][3] = Err;

	// состояние 2
	state_machine[1][0] = End;
	state_machine[1][1] = Two;
	state_machine[1][2] = Three;
	state_machine[1][3] = Err;

	// состояние 3
	state_machine[2][0] = End;
	state_machine[2][1] = AfterThree;
	state_machine[2][2] = Three;
	state_machine[2][3] = Err;

	// состояние после 3
	state_machine[3][0] = End;
	state_machine[3][1] = Err;
	state_machine[3][2] = Err;
	state_machine[3][3] = Err;

	// состояние конец строки
	state_machine[4][0] = End;
	state_machine[4][1] = End;
	state_machine[4][2] = End;
	state_machine[4][3] = End;

	// состояние ошибка
	state_machine[5][0] = Err;
	state_machine[5][1] = Err;
	state_machine[5][2] = Err;
	state_machine[5][3] = Err;

	cout << "Enter a sequence: ";

	string buff;
	getline(cin, buff);

	// начальное состояние
	int cur_state = One;
	// переход
	int cur_shift;
	// текущий элемент
	char c = ' ';
	// текущий индекс символа в buff
	int i = 0;
	// количество текущего символа подряд!!!
	int count = 0;

	int s = buff.size();
	while (cur_state != Err && cur_state != End && i <= buff.size()){
		c = buff[i];

		switch (cur_state) {
		case One:
			if (c == '1') {
				count++;
				cur_shift = Ch_one;
			}

			if (c == '2') {
				if (count == 1) {
					// количество "2" = 1
					cur_shift = Ch_two;
				}
				else
					cur_shift = Other;
			}

			if (c == '3') {
				if (count == 1) {
					// количество "3" = 1
					cur_shift = Ch_three;
				}
				else 
					cur_shift = Other;
			}
			break;
		case Two:
			if (c == '1') {
				if (i == buff.size()-1)
					cur_shift = Ch_one;
				else
					cur_shift = Other;
			}

			if (c == '2') {
				count++;
				cur_shift = Ch_two;
			}

			if (c == '3') {
				if (count == 1) {
					// количество "3" = 1
					cur_shift = Ch_three;
				}
				else
					cur_shift = Other;
			}
			break;
		case Three:
			if (c == '1') {
				if (i == buff.size()-1)
					cur_shift = Ch_one;
				else
					cur_shift = Other;
			}

			if (c == '2') {
				// количество "2" = 1
				count = 1;
				cur_shift = Ch_two;
			}

			if (c == '3') {
				count++;
				cur_shift = Ch_three;
			}
			break;
		case AfterThree:
			// окончание строки на "1"
			if (c == '1') {
				cur_shift = Ch_one;
			}

			// после "3" "2" идти ещё одной "2" не может
			if (c == '2') {
				cur_shift = Ch_two;
			}

			// после "3" "2" идти ещё одной "3" не может
			if (c == '3') {
				cur_shift = Ch_three;
			}
			break;
		case End:
			break;
		case Err:
			break;
		}
		cur_state = state_machine[cur_state][cur_shift];
		i++;
	}

	// условие коректной записи
	// для "1": если состояние КА = One и кол-во элементов в строке >=2 [ пример: 11 ]
	// если состояние КА дошли до конца End
	if ((buff.size()>=2 && cur_state == One) || (cur_state == End && i == buff.size())) {
		cout << "\nCorrect.\n\n";
	}
	else
		cout << "\nInvalid entry\n\n";

	system("pause");
	return 0;
}